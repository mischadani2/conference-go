"""

Write two Functions:
1. make a request to use the Pexels API
2. make a request to the Open Weather API.


Then, use those functions in your views.
"""
import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import pprint

# Use the Pexels API


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    headers = {
        "Authorization": "563492ad6f9170000100000169952a7694404755b51d6a55e5d63c58"
    }
    response = requests.get(url, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

    # to test this, create a location

    # Use the Open Weather API


def get_weather_data(city, state):
    geocode = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},usa&appid=060ce5dba80b1727127c9b755b19543b"
    response = requests.get(geocode)

    lat = response.json()[0].get("lat")
    lon = response.json()[0].get("lon")

    weather_data = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid=060ce5dba80b1727127c9b755b19543b&units=imperial"
    weather_data_response = requests.get(weather_data)
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(weather_data_response.json())

    temp = weather_data_response.json().get("main").get("temp")
    weather_description = (
        weather_data_response.json().get("weather")[0].get("description")
    )
    print(type(temp))

    return {
        "temp": temp,
        "weather_description": weather_description,
    }
